﻿using System.Collections.Generic;
using System.Web.Http;
using CodingChallenge.Models;

namespace CodingChallenge.Controllers
{
    [RoutePrefix("api")]
    //[Authenticate] we'd definately want to lock this down!
    public class EmployeeController : BaseApiController
    {
        // GET api/default1
        [HttpGet]
        [Route("ListEmployees/")]
        public OperationResultViewModel<List<Employee>> Get()
        {
            /*Here we woud use some di and grab the list of employees from a repository...*/
            var ret = new List<Employee>();
            var emp = new Employee
            {
                FirstName = "David", 
                LastName = "Middlebrook", 
                Salary = 2000M, 
                FamilyMembers = new List<Person>
                {
                    new Person{FirstName = "Melissa", LastName = "MiddleBrook"},
                    new Person{FirstName = "Jared", LastName = "MiddleBrook"},
                    new Person{FirstName = "Jordan", LastName = "MiddleBrook"}
                }
            };
            ret.Add(emp);
            emp = new Employee
            {
                FirstName = "Tim",
                LastName = "Johnson",
                Salary = 2000M,
                FamilyMembers = new List<Person>
                {
                    new Person{FirstName = "Teresa", LastName = "Johnson"},
                    new Person{FirstName = "Christopher", LastName = "Johnson"},
                    new Person{FirstName = "Olivia", LastName = "Johnson"}
                }
            };
            ret.Add(emp);
            emp = new Employee
            {
                FirstName = "Randy",
                LastName = "Yandell",
                Salary = 2000M,
                FamilyMembers = new List<Person>
                {
                    new Person{FirstName = "Kim", LastName = "Yandell"},
                    new Person{FirstName = "Shauna", LastName = "Yandell"},
                    new Person{FirstName = "Carson", LastName = "Yandell"},
                    new Person{FirstName = "Tanner", LastName = "Yandell"}
                }
            };
            ret.Add(emp);
            emp = new Employee
            {
                FirstName = "Gracin",
                LastName = "Albers",
                Salary = 2000M,
                FamilyMembers = new List<Person>
                {
                    new Person{FirstName = "Julie", LastName = "Albers"},
                    new Person{FirstName = "Matthew", LastName = "Albers"},
                    new Person{FirstName = "Casey", LastName = "Albers"},
                    new Person{FirstName = "Ron", LastName = "Albers"},
                    new Person{FirstName = "Shannon", LastName = "Albers"}
                }
            };
            ret.Add(emp);

            return OperationSucceeded("Success!", ret);
        }

        // GET api/default1/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/default1
        public void Post([FromBody]string value)
        {
        }

        // PUT api/default1/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/default1/5
        public void Delete(int id)
        {
        }
    }
}
