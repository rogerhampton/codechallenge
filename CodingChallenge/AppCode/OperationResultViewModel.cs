﻿using System.Collections.Generic;

namespace CodingChallenge
{
    /// <summary>
    /// This class is used when there is a need to pass error info in TempData
    /// </summary>
    public class OperationResultViewModel
    {
        public OperationResultViewModel ()
        {
            ErrorMessages = new List<string>();
            WarningMessages = new List<string>();
        }

        /// <summary>
        /// Status of the operation if it succeeded or failed
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Message displayed when an operation is successfully completed
        /// </summary>
        public string SucessMessage { get; set; }

        /// <summary>
        /// Title displayed for error list, if there are no errors on list only title is displayed.
        /// </summary>
        public string ErrorTitle { get; set; }

        /// <summary>
        /// This will be rest to true when we want the AJAX call to redirect to a specific page, this and
        ///  RedirestUrl will be used on the client side to redirect the user to another page.
        /// </summary>
        public bool IsRedirect
        {
            get { return !string.IsNullOrWhiteSpace(RedirectUrl); }
        }

        /// <summary>
        /// In-case of JSON since we cannot make a redirect using http header we need to set this is JSON response
        ///  which the code can use to redirect the user to a specific page.
        /// </summary>
        public string RedirectUrl { get; set; }

        /// <summary>
        /// This property would be used in-case we are sending a JSON response to the client.
        /// </summary>
        public IList<string> ErrorMessages { get; set; }

        /// <summary>
        /// This property is used to display warning messages, if the severity of the message is warning.
        /// </summary>
        public IList<string> WarningMessages { get; set; }

    }

    public class OperationResultViewModel<T> : OperationResultViewModel where T : new()
    {
        public T Data { get; set; }

        public OperationResultViewModel()
            : base()
        {
            Data = new T();
        }

    }
}