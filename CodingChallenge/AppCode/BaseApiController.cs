﻿using System.Collections.Generic;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace CodingChallenge
{
    [NoCacheOnBrowser]
    public abstract class BaseApiController : ApiController
    {
        protected AutoMapViewResult AutoMapView<TDestination>(object domainModel, ViewResultBase viewResult)
        {
            return new AutoMapViewResult(domainModel.GetType(), typeof (TDestination), domainModel, viewResult);
        }

        /// <summary>
        ///     Generic methiod to get header values and objects.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public T GetValueFromHeader<T>(string key)
        {
            var httpContext = (HttpContextWrapper) Request.Properties["MS_HttpContext"];
            return (T) httpContext.Items[key];
        }

        /// <summary>
        ///     Used when a Json response needs to be sent to the client, this function uses JSON.Net serializer.
        /// </summary>
        /// <param name="data">Object that needs to be serialized</param>
        /// <returns>JsonNewResult instance which does the serialization.</returns>
        protected static OperationResultViewModel OperationFailed(string errorTitle)
        {
            return OperationFailed(errorTitle, new List<string>());
        }

        protected static OperationResultViewModel<T> OperationFailed<T>(string errorTitle, T data)
            where T : new()
        {
            return OperationFailed(errorTitle, new List<string>(), data);
        }

        protected static OperationResultViewModel OperationFailed(string errorTitle, IList<string> errors)
        {
            return new OperationResultViewModel
            {
                Success = false,
                ErrorTitle = errorTitle,
                ErrorMessages = errors
            };
        }

        protected static OperationResultViewModel<T> OperationFailed<T>(string errorTitle, IList<string> errors, T data)
            where T : new()
        {
            return new OperationResultViewModel<T>
            {
                Success = false,
                ErrorTitle = errorTitle,
                ErrorMessages = errors,
                Data = data
            };
        }

        protected static OperationResultViewModel OperationSucceeded(string successMessage)
        {
            return OperationSucceeded(successMessage, null);
        }

        protected static OperationResultViewModel<T> OperationSucceeded<T>(string successMessage, T data)
            where T : new()
        {
            return OperationSucceeded(successMessage, null, data);
        }

        protected static OperationResultViewModel OperationSucceeded(string successMessage, string redirectUrl)
        {
            return new OperationResultViewModel
            {
                Success = true,
                SucessMessage = successMessage,
                RedirectUrl = redirectUrl
            };
        }

        protected static OperationResultViewModel<T> OperationSucceeded<T>(string successMessage, string redirectUrl,
            T data)
            where T : new()
        {
            return new OperationResultViewModel<T>
            {
                Success = true,
                SucessMessage = successMessage,
                RedirectUrl = redirectUrl,
                Data = data
            };
        }

        protected static OperationResultViewModel OperationSucceededWithWarnings(string successMessage,
            IList<string> warningMessages)
        {
            return OperationSucceededWithWarnings(successMessage, warningMessages, null);
        }

        protected static OperationResultViewModel<T> OperationSucceededWithWarnings<T>(string successMessage,
            IList<string> warningMessages, T data)
            where T : new()
        {
            return OperationSucceededWithWarnings(successMessage, warningMessages, null, data);
        }

        protected static OperationResultViewModel OperationSucceededWithWarnings(string successMessage,
            IList<string> warningMessages, string redirectUrl)
        {
            return new OperationResultViewModel
            {
                Success = true,
                SucessMessage = successMessage,
                RedirectUrl = redirectUrl,
                WarningMessages = warningMessages
            };
        }

        protected static OperationResultViewModel<T> OperationSucceededWithWarnings<T>(string successMessage,
            IList<string> warningMessages, string redirectUrl, T data)
            where T : new()
        {
            return new OperationResultViewModel<T>
            {
                Success = true,
                SucessMessage = successMessage,
                RedirectUrl = redirectUrl,
                WarningMessages = warningMessages,
                Data = data
            };
        }
    }
}