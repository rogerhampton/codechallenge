﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace CodingChallenge
{
    //A fairly simple self explanitory class used to convert data form one type to another safely.
    public static class DataConvert
    {

        public static T To<T>(this object value) where T : IConvertible
        {
            return To(value, default(T));
        }

        public static T To<T>(this object value, T defaultValue) where T : IConvertible
        {
            if (IsNull(value)) return defaultValue;
            if (typeof(T) == typeof(bool)) return (T)(object)ToBool(value);
            if (typeof(T) == typeof(short)) return (T)(object)ToShort(value);
            if (typeof(T) == typeof(int)) return (T)(object)ToInt(value);
            if (typeof(T) == typeof(long)) return (T)(object)ToLong(value);
            if (typeof(T) == typeof(byte)) return (T)(object)ToByte(value);
            if (typeof(T) == typeof(decimal)) return (T)(object)ToDecimal(value);
            if (typeof(T) == typeof(double)) return (T)(object)ToDouble(value);
            var converter = TypeDescriptor.GetConverter(typeof(T));
            try
            {
                return (T)converter.ConvertFromString(value.ToString());
            }
            catch
            {
                return defaultValue;
            }
        }

        private static short ToShort(this object value)
        {
            try
            {
                return short.Parse(value.ToString(), NumberStyles.Any);
            }
            catch
            {
                return default(short);
            }
        }

        private static int ToInt(this object value)
        {
            try
            {
                return int.Parse(value.ToString(), NumberStyles.Any);
            }
            catch
            {
                return default(int);
            }
        }

        private static long ToLong(this object value)
        {
            try
            {
                return long.Parse(value.ToString(), NumberStyles.Any);
            }
            catch
            {
                return default(long);
            }
        }

        private static byte ToByte(this object value)
        {
            try
            {
                return byte.Parse(value.ToString(), NumberStyles.Any);
            }
            catch
            {
                return default(byte);
            }
        }

        private static decimal ToDecimal(this object value)
        {
            try
            {
                return decimal.Parse(value.ToString(), NumberStyles.Any);
            }
            catch
            {
                return default(decimal);
            }
        }

        private static double ToDouble(this object value)
        {
            try
            {
                return double.Parse(value.ToString(), NumberStyles.Any);
            }
            catch
            {
                return default(double);
            }
        }

        private static bool ToBool(this object value)
        {
            if (IsNull(value)) return false;
            if (value.ToString().Length == 0) return false;
            switch (value.ToString().ToLower()[0])
            {
                case 'f':
                case 'n':
                case '0':
                    return false;
                default:
                    return true;
            }
        }

        public static byte[] ToByteArray(this object value)
        {
            if (IsNull(value)) return new byte[] { };
            try
            {
                return (byte[])value;
            }
            catch (InvalidCastException)
            {
                return new byte[] { };
            }
        }

        public static bool IsNull(this object value)
        {
            return value == null || value == DBNull.Value;
        }
    }
}