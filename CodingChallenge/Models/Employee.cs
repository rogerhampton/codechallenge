﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodingChallenge.Models
{
    public class Employee: Person
    {
        public decimal Salary { get; set; }
        public List<Person> FamilyMembers { get; set; }

    }
}