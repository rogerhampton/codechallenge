﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;


namespace CodingChallenge.Filters
{
    public class AuthenticateAttribute: AuthorizationFilterAttribute
    {
        public override void OnAuthorization(System.Web.Http.Controllers.HttpActionContext actionContext)
        {

            // Does the request headers contain the Authentication property?
            if (actionContext.Request.Headers.Contains("Authentication"))
            {
                // Let's grab the token from the request header Authentication property
                var token = actionContext.Request.Headers.GetValues("Authentication").First();


                // If that token is NOT empty
                if (String.IsNullOrWhiteSpace(token) == false)
                {
                    //Do some authentication stuff.
                }
                else
                    // Then send back Http Forbidden. This user is not authorized
                    actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Authentication token is not present");

            }
            else
                // Then send back Http Forbidden. This user is not authorized
                actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Authentication header is not present");




            base.OnAuthorization(actionContext);
        }
    }
}