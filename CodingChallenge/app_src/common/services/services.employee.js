/**
 * Created by rhampton on 7/11/2014.
 */
angular.module('services.employee', ['core.ajax']);

angular.module('services.employee',[]).service('employeeService', function($q, apiService) {
    'use strict';
    function execute(apiSvcSettings, deferred){
        apiService.execute(apiSvcSettings).then(
            function (data)
            {
                deferred.resolve(data.Data);
            },
            function (error)
            {
                deferred.reject(error.ErrorMessages);
            }
        );
    }
    return {
        loadEmployees: function ()
        {
            var deferred = $q.defer();
            var apiSvcSettings = new ApiServiceSettings(
                {
                    method: 'GET',
                    url: '../api/ListEmployees/',
                    data: {
                    }
                }
            );
            execute(apiSvcSettings, deferred);
            return deferred.promise;
        }
    };
});