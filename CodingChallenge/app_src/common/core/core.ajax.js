/**
 * Created by Roger Hampton on 4/15/15.
 */
/* ==============================================================================

 CORE.AJAX MODULE

 Defines the core.ajax module and it all it's module dependencies

 ============================================================================= */
function ApiServiceSettings(settings) {
    this.url = settings.url;
    this.data = settings.data;
    this.method = settings.method;
    this.params = settings.params;
    this.responseType = settings.responseType;
    this.scope = settings.scope; // if set then this is the scope that validations will execute for
    this.skipValidations = settings.skipValidations || false; // if true then POST or PUT validations eval is skipped
}
function ApiServiceOperationResult(data) {
    this.Success = data.Success;
    this.SuccessMsg = data.SucessMessage;
    this.ErrorTitle = data.ErrorTitle;
    this.Errors = data.ErrorMessages || [];
    this.Warnings = data.WarningMessages || [];
    this.HasErrors = (this.Errors.length > 0);
    this.HasWarnings = (this.Warnings.length > 0);
    this.IsRedirect = data.IsRedirect;
    this.RedirectUrl = data.RedirectUrl;
    this.Data = data.Data;
}


angular.module('core.ajax', [])



    /* ==============================================================================

     CORE.AJAX

     Contains helpful methods that are common to most ajax/xhr commands.

     ============================================================================= */
    .factory('ajax', ['$http', function ($http) {
        'use strict';

        return {

            /*
             GET
             These are the very simplified extensions of angular's $http method. I prefer what I created below.
             */
            get: function (url, data) {

                if (data === undefined) {

                    return $http({
                        method: 'GET',
                        url: url
                    });

                } else {

                    return $http({
                        method: 'GET',
                        url: url + '/' + data
                    });

                }

            },


            /*
             POST
             */
            post: function (url, data) {

                return $http({
                    method: 'POST',
                    url: url,
                    data: data,
                    headers: {
                        "Content-Type": "application/json"
                    }
                });

            }

        };

    }])
    .service('apiService',
    ['$http', '$q', '$window',
        function($http, $q, $location) {
            function webAPICall(settings, notifyOnSuccess, suppressErrors){
                var opResults = {};

                var deferred = $q.defer();

                //Ideally we would do some validations and use this as a last gasp to stop a bad post.
                // for POST and PUT execute validation on data
                //if ((settings.method === 'POST' || settings.method === 'PUT') && !settings.skipValidations) {
                //    if (!validatorSvc.executeValidations()) {
                //        return deferred.promise;
                //    }
                //}

                $http({
                    method: settings.method || 'GET',
                    url: settings.url,
                    data: settings.data,
                    params: settings.params
                    //responseType: settings.responseType || 'json' // ie 8/9 angularjs 1.2.6 issue https://github.com/angular/angular.js/issues/4738
                })
                    .success(function(data, status, headers, config) {

//                        opResults = new ApiServiceOperationResult(data.Data);
                        opResults = new ApiServiceOperationResult(data);

                        if (opResults.IsRedirect) {
                            $location.path = opResults.RedirectUrl; //follow redirect
                        }

                        if (opResults.Success) {
                            //Here we would put a service to notify the user the the request was successful
                            //if(notifyOnSuccess)
                                //NotificationService.handleSuccess(settings, opResults, status, headers, config);
                            deferred.resolve(opResults, status, headers, config);
                        } else {
                            //Here we would put a service to notify the user the the request failed
                            //if(!suppressErrors)
                            //    NotificationService.handleFailure(settings, opResults, status, headers, config);
                            deferred.reject(opResults, status, headers, config);
                        }
                    })
                    .error(function(data, status, headers, config) {
                        opResults = new ApiServiceOperationResult(data.Data);
                        //Here we would put a service to notify the user the the request failed
                        //if(!suppressErrors)
                        //    NotificationService.handleFailure(settings, opResults, status, headers, config);
                        deferred.reject(opResults, status, headers, config);
                    });

                return deferred.promise;
            }
            return {
                /*
                * These overrrides are in place to controll how the user is notifed.  Not using on this test app.
                * */
                //executeAndNotifyOnSuccess: function(settings) {
                //    return webAPICall(settings, true, false);
                //},
                //executeAndNotifyOnSuccessAndSuppressErrors: function(settings) {
                //    return webAPICall(settings, true, true);
                //},
                execute:function(settings){
                    return webAPICall(settings, false, false);
                }
                //executeAndSuppressErrors:function(settings){
                //    return webAPICall(settings, false, true);
                //}
            };
        }
    ]);
