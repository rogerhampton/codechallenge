/**
 * Created by rhampton on 6/17/2014.
 */


/* ==============================================================================

 CORE.DIRECTIVES MODULE

 Defines the module namespace for the core.directives

 ============================================================================= */
angular.module('core.directives', []);

/* ==============================================================================

 Modal Dialog

 Shows and hides a modal dialog centered on the page.

 ============================================================================= */
angular.module('core.directives')
    .directive("toolTips", function ($compile)
    {
        var clone = $compile('<div class="summaryerror"><div class="field-validation-error"><div>{{text}}</div></div></div>');

        function link(scope, el, attr)
        {
            if (scope.text && scope.text.length > 0)
            {
                el.qtip({
                    position: {
                        at: "bottom left"
                    },
                    style: {
                        tip: {
                            corner: "top center"
                        }
                    },
                    content: {
                        text: function ()
                        {
                            return scope.$apply(function ()
                            {
                                return clone(scope);
                            });
                        }
                    }
                });
            }
        }

        return {
            restrict: 'A',
            link: link,
            scope: {
                text: "=toolTips"
            }
        };
    });

