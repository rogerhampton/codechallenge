/**
 * Created by Roger Hampton on 4/15/15.
 */
angular.module("codeChallengeApp", [
    'ngRoute',
    'employee',
    'core.ajax',
    'core.directives',
    'services.employee'
])
    .config(function ($routeProvider, $httpProvider)
    {
        $routeProvider
            .when("/",
            {
                templateUrl: 'login/login.tpl.html',
                controller: 'loginCtrl'
            }
        );

        // Add our custom Interceptors
        $httpProvider.interceptors.push('httpInterceptor');

        //We'd definately add some authorization tokens to any headers for WebAPI calls.
        //$httpProvider.interceptors.push('AuthInterceptor');

        // For backwards compatibility to old browsers, set this to false
        //$locationProvider.html5Mode(false).hashPrefix('!');

    })
    .factory('httpInterceptor', function ($rootScope, $q, $injector)
    {

        return {

            request: function (config)
            {

                config.headers = config.headers || {};

                //We'd need to do some work here to let the user
                //know that some work was being done.
                // Show our ajax spinner
                //$rootScope.showLoader = true;

                return config || $q.when(config);
            },

            response: function (response)
            {

                var $http = $http || $injector.get('$http');

                //Comming back we'd need to turn the spinner off once the web api call was done.
                // If all the pending http requests are done or
                // the user is not authenticated
                //if ($http.pendingRequests.length < 1 || $rootScope.isAuthenticated === false)
                //{
                //    // Hide the ajax spinner
                //    $rootScope.showLoader = false;
                //}

                return response || $q.when(response);

            },

            responseError: function (response)
            {
                //We'd also need a place to handle the basic errors
                //switch(response.status)
                //{
                //    case 500:
                // }
                //
                //// An error has occurred, hide the ajax spinner
                //$rootScope.showLoader = false;

                return $q.reject(response);
            }

        };

    })
    .controller("indexCtrl", function ($scope, $http, $q, $rootScope)
    {
        'use strict';

        $scope.initialize = function ()
        {

        };
    });
