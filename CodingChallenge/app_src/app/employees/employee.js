/**
 * Created by Roger on 4/15/2015.
 */

angular.module('employee', [])
    .config(function($routeProvider){
        $routeProvider.when('/Employees/List',{
            templateUrl: 'app/employees/employee.tpl.html',
            controller: 'listEmployeesCtrl'
        })
    })
    .controller('listEmployeesCtrl', function($scope, employeeService){
        /*
        * Ideally these costs wouuld come from a db...
        * */
        $scope.EmpInsCost = 1000.00;
        $scope.FamInsCost = 500.00;
        $scope.discretionaryDiscount = 0.10;
        $scope.discretionaryRule = /\ba/gi;
        $scope.initialize = function(){
            $scope.employees = {};
            $scope.predicate='LastName';
            $scope.reverse=false;
            employeeService.loadEmployees().then(function(data){
                $scope.employees = data;
                $scope.calculateCosts();
            },function(err){
                //Here we'd do some defensive/reactive coding to contain the damage.
            });
            $scope.smoke='Smoke Test';
        };

        //Just a quick method to load up a currently selected employee.
        $scope.setActiveEmployee = function(employee){
          $scope.activeEmp = employee;
        };

        $scope.calculateCosts = function(){
            angular.forEach($scope.employees, function(emp){
                emp.familyCost = (emp.FamilyMembers.length * $scope.FamInsCost);
                emp.totalAnnualCost = $scope.EmpInsCost + emp.familyCost;
                emp.empAnnualCost = $scope.EmpInsCost;
                if(emp.FirstName.match($scope.discretionaryRule) || emp.LastName.match($scope.discretionaryRule)){
                    var discount = 1.0-$scope.discretionaryDiscount;
                    emp.totalAnnualCost = emp.totalAnnualCost * discount;
                    emp.familyCost = emp.familyCost * discount;
                    emp.empAnnualCost = $scope.EmpInsCost * discount;
                }
                emp.biWeeklyCost = emp.totalAnnualCost / 26.0;
                emp.biWeeklyEmpCost = emp.empAnnualCost / 26.0;
                emp.biWeeklyFamilyCost = emp.familyCost / 26.0;
            });
        };
    });